﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DemoObserver : MonoBehaviour
{
    public abstract bool IsRunning { get; }

    public abstract void OnNotifyClick();
    public abstract void OnNotifySetDuration(float duration);
    public abstract void OnNotifySetDelay(float delay);
    public abstract void OnNotifySetColor(Color color);
    public abstract void OnNotifyScaledTime(bool scaledTime);
}
