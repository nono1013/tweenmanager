﻿using nono1013.Tween;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoBg2Color : DemoObserver
{
    public float _duration = 1f;
    public Color _color = Color.white;
    public bool _scaledTime = true;

    public TweenColor _tweenColor;
    TweenInstance<Color> _tweenCol;

    SpriteRenderer _circleRenderer;

    void Awake()
    {
        _circleRenderer = transform.GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        _tweenCol = new TweenInstance<Color>(_duration, _tweenColor, (c) => _circleRenderer.color = c, () => { }, _scaledTime, 0f);
    }

    public override void OnNotifyClick()
    {
        _tweenCol.Start = _circleRenderer.color;
        _tweenCol.End = _color;
        _tweenCol.StartTween(this);
    }

    public override void OnNotifySetDuration(float duration)
    {
        _tweenCol.Duration = duration;
    }

    public override void OnNotifySetDelay(float delay)
    {

    }

    public override void OnNotifySetColor(Color color)
    {
        _color = Color.white - color;
        _color.a = 1f;
    }

    public override void OnNotifyScaledTime(bool scaledTime)
    {
        _tweenCol.ScaledTime = scaledTime;
    }

    public override bool IsRunning
    {
        get { return _tweenCol.IsRunning; }
    }
}
