﻿using nono1013.Tween;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DemoTweenController : MonoBehaviour
{
    List<DemoObserver> _observers = new List<DemoObserver>();
    
    public TextMesh _durationText;
    public TextMesh _delayText;
    public TextMesh _timescaleText;

    void Awake()
    {
        /// Get all observers.
        _observers.AddRange(GameObject.FindObjectsOfType<DemoObserver>());
    }

    void Update()
    {
        /// If mouse is clicked ...
        if (Input.GetMouseButtonDown(0))
        {
            /// ... then find mouse position on world, ...
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            /// ... if observer tweens are not running and collider is clicked ...
            if (!_observers.Any(o => o.IsRunning) && Physics2D.Raycast(mousePos, Vector2.zero, 0f))
            {
                /// ... then notify click.
                NotifyClick();
            }
            
        }
    }

    public void NotifyClick()
    {
        /// Update for random colors.
        NotifySetColor(UnityEngine.Random.ColorHSV());

        /// Go through observers and notify click.
        _observers.ForEach(o => o.OnNotifyClick());
    }

    /// <summary>
    /// Notify duration change for observers and set UI text.
    /// </summary>
    /// <param name="slider"></param>
    public void NotifySetDuration(Slider slider)
    {
        _durationText.text = "Duration: " + String.Format("{0:0.00}", slider.value);
        _observers.ForEach(o => o.OnNotifySetDuration(slider.value));
    }

    /// <summary>
    /// Notify delay change for observers and set UI text.
    /// </summary>
    /// <param name="slider"></param>
    public void NotifySetDelay(Slider slider)
    {
        _delayText.text = "Delay: " + String.Format("{0:0.00}", slider.value);
        _observers.ForEach(o => o.OnNotifySetDelay(slider.value));
    }

    /// <summary>
    /// Notify color change for observers and set UI text.
    /// </summary>
    /// <param name="color"></param>
    public void NotifySetColor(Color color)
    {
        _observers.ForEach(o => o.OnNotifySetColor(color));
    }

    /// <summary>
    /// Notify scale time change for observers and set UI text.
    /// </summary>
    /// <param name="color"></param>
    public void NotifySetScaledTime(Toggle toggle)
    {
        _observers.ForEach(o => o.OnNotifyScaledTime(toggle.isOn));
    }

    /// <summary>
    /// Set global time scale.
    /// </summary>
    /// <param name="slider"></param>
    public void SetGlobalTimeScale(Slider slider)
    {
        _timescaleText.text = "Global time scale: " + String.Format("{0:0.00}", slider.value);
        Time.timeScale = slider.value;
    }
}
