﻿using nono1013.Tween;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DemoBgContainer : DemoObserver
{
    public float _duration = 1f;
    public bool _scaledTime = true;

    public TweenFloat _tweenFloat;
    TweenInstance<float> _tweenRot;

    void Start()
    {
        _tweenRot = new TweenInstance<float>(0f, 360f, _duration, _tweenFloat, (v) => transform.rotation = Quaternion.Euler(0f, 0f, v), () => { }, _scaledTime, 0f);
    }
	
	public override void OnNotifyClick()
    {
        _tweenRot.StartTween(this);
    }

    public override void OnNotifySetDuration(float duration)
    {
        _tweenRot.Duration = duration;
    }

    public override void OnNotifySetDelay(float delay)
    {

    }

    public override void OnNotifySetColor(Color color)
    {

    }

    public override void OnNotifyScaledTime(bool scaledTime)
    {
        _tweenRot.ScaledTime = scaledTime;
    }

    public override bool IsRunning
    {
        get { return _tweenRot.IsRunning; }
    }
}
