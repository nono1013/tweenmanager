﻿using nono1013.Tween;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoBallPosition : DemoObserver
{
    public float _duration = 1f;
    public float _delay = 0.2f;
    public bool _scaledTime = true;

    public TweenVector2 _tweenVector2In;
    public TweenVector2 _tweenVector2Out;
    
    TweenInstance<Vector2> _tweenIn;
    TweenInstance<Vector2> _tweenOut;
    
    int m_Direction = 1;
    
    void Start()
    {
        _tweenIn = new TweenInstance<Vector2>(_duration, _tweenVector2In, (v2) => transform.position = v2, () => { }, _scaledTime, _delay);
        _tweenOut = new TweenInstance<Vector2>(_duration, _tweenVector2Out, (v2) => transform.position = v2, () => { }, _scaledTime, _delay);

        /// We could add the callback above in the instantiation, but it's easier to put it here.
        _tweenIn.Callback = () =>
        {
            /// We want _tweenOut to start after _tweenIn ends.
            _tweenOut.Start = transform.position;
            _tweenOut.End = transform.position - Vector3.up;
            _tweenOut.StartTween(this);
        };
    }

    public override void OnNotifyClick()
    {
        _tweenIn.Start = transform.position;
        _tweenIn.End = transform.position + Vector3.up;
        _tweenIn.StartTween(this);
    }

    public override void OnNotifySetDuration(float duration)
    {
        _tweenIn.Duration = duration;
        _tweenOut.Duration = duration;
    }

    public override void OnNotifySetDelay(float delay)
    {
        _tweenOut.Delay = delay;
    }

    public override void OnNotifySetColor(Color color)
    {

    }

    public override void OnNotifyScaledTime(bool scaledTime)
    {
        _tweenIn.ScaledTime = scaledTime;
        _tweenOut.ScaledTime = scaledTime;
    }

    public override bool IsRunning
    {
        get { return _tweenIn.IsRunning || _tweenOut.IsRunning; }
    }
}
