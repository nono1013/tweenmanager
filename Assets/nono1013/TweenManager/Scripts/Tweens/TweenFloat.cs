﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nono1013.Tween
{
    [System.Serializable]
    public class TweenFloat : Tween<float>
    {
        public AnimationCurve Value = AnimationCurve.Linear(0, 0, 1, 1);
        
        public override float Evaluate(float time)
        {
            return Value.Evaluate(time);
        }

        public override float Distance(float start, float end)
        {
            return end - start;
        }

        public override float Scale(float a, float b)
        {
            return a * b;
        }

        public override float Add(float a, float b)
        {
            return a + b;
        }
    }
}