﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nono1013.Tween
{
    [System.Serializable]
    public class TweenVector3 : Tween<Vector3>
    {
        public AnimationCurve X = AnimationCurve.Linear(0, 0, 1, 1);
        public AnimationCurve Y = AnimationCurve.Linear(0, 0, 1, 1);
        public AnimationCurve Z = AnimationCurve.Linear(0, 0, 1, 1);
        
        public override Vector3 Evaluate(float time)
        {
            return new Vector3(X.Evaluate(time), Y.Evaluate(time), Z.Evaluate(time));
        }

        public override Vector3 Distance(Vector3 start, Vector3 end)
        {
            return end - start;
        }

        public override Vector3 Add(Vector3 start, Vector3 end)
        {
            return start + end;
        }

        public override Vector3 Scale(Vector3 a, Vector3 b)
        {
            return Vector3.Scale(a, b);
        }
    }
}