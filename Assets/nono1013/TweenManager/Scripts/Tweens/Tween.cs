﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nono1013.Tween
{
    public interface ITween<T>
    {
        T Evaluate(float time);
        T Distance(T start, T end);
        T Scale(T a, T b);
        T Add(T a, T b);
    }
    
    public abstract class Tween<T> : ITween<T>
    {
        public abstract T Add(T a, T b);

        public abstract T Distance(T start, T end);

        public abstract T Evaluate(float time);

        public abstract T Scale(T a, T b);
    }
}