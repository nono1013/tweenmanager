﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nono1013.Tween
{
    [System.Serializable]
    public class TweenVector2 : Tween<Vector2>
    {
        public AnimationCurve X = AnimationCurve.Linear(0, 0, 1, 1);
        public AnimationCurve Y = AnimationCurve.Linear(0, 0, 1, 1);

        public override Vector2 Evaluate(float time)
        {
            return new Vector2(X.Evaluate(time), Y.Evaluate(time));
        }

        public override Vector2 Distance(Vector2 start, Vector2 end)
        {
            return end - start;
        }

        public override Vector2 Add(Vector2 start, Vector2 end)
        {
            return start + end;
        }

        public override Vector2 Scale(Vector2 a, Vector2 b)
        {
            return Vector2.Scale(a, b);
        }
    }
}