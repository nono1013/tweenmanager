﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nono1013.Tween
{
    [System.Serializable]
    public class TweenColor : Tween<Color>
    {
        public AnimationCurve R = AnimationCurve.Linear(0, 0, 1, 1);
        public AnimationCurve G = AnimationCurve.Linear(0, 0, 1, 1);
        public AnimationCurve B = AnimationCurve.Linear(0, 0, 1, 1);
        public AnimationCurve A = AnimationCurve.Linear(0, 0, 1, 1);
        
        public override Color Evaluate(float time)
        {
            return new Color(R.Evaluate(time), G.Evaluate(time), B.Evaluate(time), A.Evaluate(time));
        }

        public override Color Distance(Color start, Color end)
        {
            return end - start;
        }

        public override Color Add(Color start, Color end)
        {
            return start + end;
        }

        public override Color Scale(Color a, Color b)
        {
            return a * b;
        }
    }
}