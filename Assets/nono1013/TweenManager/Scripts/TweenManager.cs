﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nono1013.Tween
{
    public class TweenManager : MonoBehaviour
    {
        #region Singleton
        static TweenManager _instance;

        public static TweenManager Instance
        {
            get {
                if (_instance == null)
                    _instance = new GameObject().AddComponent<TweenManager>();
                return _instance;
            }
        }

        private TweenManager() { }
        #endregion
        
        public void StartTween<T>(T start, T end, float duration, Tween<T> tween, Action<T> update, Action callback = null, bool scaledTime = true, MonoBehaviour target = null)
        {
            TweenInstance<T> tweenInstance = new TweenInstance<T>(start, end, duration, tween, update, callback, scaledTime);
            tweenInstance.StartTween(target == null ? this : target);
        }
    }
}