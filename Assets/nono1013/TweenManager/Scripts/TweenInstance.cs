﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nono1013.Tween
{
    public class TweenInstance<T>
    {
        T _start;
        T _end;
        float _duration;
        Tween<T> _tween;
        Action<T> _update;

        Action _callback;
        bool _scaledTime;
        float _delay;

        MonoBehaviour _target;

        Coroutine _coroutine;
        bool _isRunning;
        
        public TweenInstance(T start, T end, float duration, Tween<T> tween, Action<T> update, Action callback = null, bool scaledTime = true, float delay = 0f)
            : this(duration, tween, update, callback, scaledTime, delay)
        {
            _start = start;
            _end = end;
        }

        public TweenInstance(float duration, Tween<T> tween, Action<T> update, Action callback = null, bool scaledTime = true, float delay = 0f)
        {
            _duration = duration;
            _delay = delay;
            _tween = tween;
            _update = update;
            _callback = callback;
            _scaledTime = scaledTime;

            _isRunning = false;
        }

        public void StartTween(MonoBehaviour target)
        {
            if (!_isRunning)
            {
                _target = target;
                _coroutine = _target.StartCoroutine(UpdateTween());
                _isRunning = true;
            }
        }

        public void StopTween()
        {
            if (_target != null && _coroutine != null)
            {
                _target.StopCoroutine(_coroutine);
                _coroutine = null;
            }
            _isRunning = false;
        }

        IEnumerator UpdateTween()
        {
            T distance = _tween.Distance(_start, _end);
            float counter = 0f;

            while (true)
            {
                if (_scaledTime)
                    counter += Time.deltaTime;
                else
                    counter += Time.unscaledDeltaTime;

                if (counter < _delay)
                    yield return null;

                if (counter >= _duration + _delay)
                    break;

                float current = (counter - _delay) / _duration;

                T unit = _tween.Evaluate(current);
                _update(_tween.Add(_start, _tween.Scale(distance, unit)));

                yield return null;
            }

            _update(_end);

            if (_callback != null)
                _callback();

            _isRunning = false;
        }

        public T Start
        {
            get { return _start; }
            set { _start = value; }
        }

        public T End
        {
            get { return _end; }
            set { _end = value; }
        }

        public float Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public float Delay
        {
            get { return _delay; }
            set { _delay = value; }
        }

        public Tween<T> Tween
        {
            get { return _tween; }
            set { _tween = value; }
        }

        public Action<T> Update
        {
            get { return _update; }
            set { _update = value; }
        }

        public Action Callback
        {
            get { return _callback; }
            set { _callback = value; }
        }

        public bool ScaledTime
        {
            get { return _scaledTime; }
            set { _scaledTime = value; }
        }

        public MonoBehaviour Target
        {
            get { return _target; }
            set { _target = value; }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
        }
    }
}